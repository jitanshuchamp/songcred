package com.tcs.songmvvm.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tcs.songmvvm.R
import com.tcs.songmvvm.repo.Songs
import com.tcs.songmvvm.ui.home.HomeFragment
import kotlinx.android.synthetic.main.custom_view.view.*

class SongsAdapter(
    val students: List<Songs>,
    homeFragment: HomeFragment
)
    : RecyclerView.Adapter<SongsAdapter.ViewHolder>(){

    private var listener: OnItemClickListener? = homeFragment


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : SongsAdapter.ViewHolder {
        val v: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.custom_view,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: SongsAdapter.ViewHolder, position: Int) {
        holder.id.text = students[position].song
        holder.name.text = students[position].artists
        //Click listener for card
        holder.card_viewMain.setOnClickListener { view ->
            listener?.onItemClickShowDetails(students[position], position)
        }

    }

    override fun getItemCount(): Int {
        return students.size
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val id = itemView.tvId
        val name = itemView.tvName
        val card_viewMain = itemView.card_viewMain
    }

    interface OnItemClickListener {
        fun onItemClickShowDetails(songs: Songs, position: Int)

    }
}