package com.tcs.songmvvm.ui.home

import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.squareup.picasso.Picasso
import com.tcs.songmvvm.R
import com.tcs.songmvvm.repo.Songs
import com.tcs.songmvvm.ui.home.adapter.SongsAdapter

class HomeFragment : Fragment(), SongsAdapter.OnItemClickListener, View.OnClickListener {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var slidingLayout: SlidingUpPanelLayout
    private lateinit var ivSong: ImageView
    private lateinit var ivSongCenter: ImageView
    private lateinit var ivPlay: ImageView
    private lateinit var ivPlayNext: ImageView
    private lateinit var ivPlayLast: ImageView
    private lateinit var tvSong: TextView
    var mediaPlayer: MediaPlayer = MediaPlayer()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        val rvSongs: RecyclerView = root.findViewById(R.id.rv_songs)

        //set layout slide listener
        slidingLayout = root.findViewById(R.id.sliding_layout)
        ivSong = root.findViewById(R.id.iv_song)
        ivSongCenter = root.findViewById(R.id.iv_song_center)
        ivPlay = root.findViewById(R.id.iv_play)
        ivPlayNext = root.findViewById(R.id.iv_forward)
        ivPlayLast = root.findViewById(R.id.iv_backword)
        tvSong = root.findViewById(R.id.tv_song)

        slidingLayout.setPanelSlideListener(onSlideListener())


        setOnClickListener()
        // Specify layout for recycler view
        val linearLayoutManager = LinearLayoutManager(
            context, RecyclerView.VERTICAL,false)
        rvSongs.layoutManager = linearLayoutManager

        homeViewModel.getSongs()

        homeViewModel.getSongsList()?.observe(this, Observer {
            rvSongs.adapter = SongsAdapter(it, this)
        })
        return root
    }

    private fun setOnClickListener() {
        ivPlayLast.setOnClickListener(this)
        ivPlayNext.setOnClickListener(this)
        ivPlay.setOnClickListener(this)
    }

    private fun onSlideListener(): SlidingUpPanelLayout.PanelSlideListener {
        return object : SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(view: View, v: Float) {
            }

            override fun onPanelCollapsed(view: View) {
            }

            override fun onPanelExpanded(view: View) {
            }

            override fun onPanelAnchored(view: View) {
            }

            override fun onPanelHidden(view: View) {
            }
        }
    }

    override fun onItemClickShowDetails(songs: Songs, position: Int) {
        showPanel(songs)
    }

    private fun showPanel(songs: Songs) {
        slidingLayout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        tvSong.text = songs.song
        val picasso = Picasso.get()
        picasso.load(songs.cover_image)
            .placeholder(R.drawable.song_image)
            .resize(100, 100)
            .into(ivSong)
        picasso.load(songs.cover_image)
            .placeholder(R.drawable.song_image)
            .into(ivSongCenter)
        val url = songs.url
        mediaPlayer = MediaPlayer().apply {
            setAudioStreamType(AudioManager.STREAM_MUSIC)
            setDataSource(url)
            prepare() // might take long! (for buffering, etc)
            start()
        }

    }

    override fun onClick(v: View?) {
        if(v?.id == R.id.iv_play){
            if(mediaPlayer.isPlaying){
                mediaPlayer.pause()
            }else{
                mediaPlayer.start()
            }
        }else if(v?.id == R.id.iv_backword){
        }else if(v?.id == R.id.iv_forward){

        }
    }
}