package com.tcs.songmvvm.ui.home.adapter

import android.view.View

interface RecyclerViewClickListener {

    fun onClick(view: View, position: Int)
}