package com.tcs.songmvvm.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tcs.songmvvm.repo.Songs
import com.tcs.songmvvm.repo.SongsRepository

class HomeViewModel : ViewModel() {
    val repository : SongsRepository = SongsRepository()

    fun getSongs() {
        repository.getSongs()
    }


    fun getSongsList() : LiveData<List<Songs>>?  {
       return repository.songsList
    }


}