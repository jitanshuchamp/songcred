package com.tcs.songmvvm.repo

import android.util.Log
import retrofit2.Call
import retrofit2.http.GET
import androidx.lifecycle.MutableLiveData
import retrofit2.Callback
import retrofit2.Response
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit


class SongsRepository {
    private var retrofit: Retrofit? = null
    private val BASE_URL = "http://starlord.hackerearth.com/"
    val songsList: MutableLiveData<List<Songs>>? = MutableLiveData()



    fun retrofit() : Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun getSongs() {
        val request = retrofit().create(SongsService::class.java)
        request.getSongs().enqueue(object : Callback<List<Songs>> {
            override fun onResponse(call: Call<List<Songs>>, response: Response<List<Songs>>) {
                songsList?.value = response.body()
            }

            override fun onFailure(response: Call<List<Songs>>, t: Throwable) {
                Log.d("error", t.localizedMessage)
            }
        })
    }
}

interface SongsService {

    @GET("studio")
    fun getSongs(
    ): Call<List<Songs>>

}