package com.tcs.songmvvm.repo

data class Songs(
    val artists: String,
    val cover_image: String,
    val song: String,
    val url: String
)